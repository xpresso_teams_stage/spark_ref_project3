
import os

from pyspark.ml.feature import VectorAssembler
from one_hot_encoder.app.abstract_component import AbstractSparkPipelineTransformer


class CustomVectorAssembler(VectorAssembler, AbstractSparkPipelineTransformer): 

    def __init__(self, name, xpresso_run_name, inputCols=None, outputCol=None):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={self.name} with xpresso_run_name {xpresso_run_name}', flush=True)
        VectorAssembler.__init__(self, inputCols=inputCols, outputCol=outputCol)
        AbstractSparkPipelineTransformer.__init__(self, xpresso_run_name)
    
    def _transform(self, dataset):
        self.state = dataset
        args = [self.xpresso_run_name]
        should_run = self.start(*args)
        print(f"In {self.__class__.__name__} _transform, now calling super's _transform", flush=True)
        ds = super()._transform(dataset)
        self.state = ds
        self.completed()
        print(f'Completed component: {self.name}', flush=True)
        return ds