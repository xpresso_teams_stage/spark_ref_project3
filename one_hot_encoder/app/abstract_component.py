import threading
import time
import os
import argparse
from datetime import datetime

from pyspark.sql import SparkSession
from pyspark.ml.pipeline import Pipeline
from pyspark.ml.pipeline import PipelineModel
from pyspark.ml.base import Estimator, Transformer
from one_hot_encoder.app.mongo import MongoPersistenceManager

from .controller_stub import ControllerStub

class AbstractPipelineComponent:
    def __init__(self, xpresso_run_name, is_thread_needed=True):
        print("Initializing component")
        # note run ID
        self.xpresso_run_name = xpresso_run_name
        # set run_status
        self.run_status = "IDLE"
        # state of componet (to be saved on pause, and loaded on restart)
        self.state = None
        # status of component (to be reported on periodic basis - consists of status dict and metrics dict)
        self.status = None
        # output of component (to be stored on disk on completion)
        self.output = None
        # final results of component (to be stored in database on completion)
        self.results = None

        self.run_status_thread = None
        self.is_thread_needed = is_thread_needed
        self.controller = ControllerStub()
        print("Component initlaized")

    def start(self, *args):
        print("Parent component starting", flush=True)
        self.xpresso_run_name = args[0]
        # check status immediately and take action if required
        self.get_run_status()

        # start thread to check run status
        self.run_status_thread = threading.Thread(target=self.check_run_status)
        self.is_thread_needed = True
        self.run_status_thread.start()
        self.controller.pipeline_component_started(self.xpresso_run_name, self.name)

    def terminate(self):
        print("Parent component terminating", flush=True)
        self.controller.pipeline_component_terminated(self.xpresso_run_name, self.name)
        self.is_thread_needed = False
        os._exit(0)

    def terminate_without_exit(self):
        print("Parent component terminating without exit", flush=True)
        self.controller.pipeline_component_terminated(self.xpresso_run_name, self.name)
        self.is_thread_needed = False

    def pause(self):
        print("Parent component saving state and exiting", flush=True)
        self.controller.pipeline_component_paused(self.xpresso_run_name, self.name,
                                                  self.state)
        self.is_thread_needed = False
        os._exit(0)

    def restart(self):
        print("Parent component restarting", flush=True)
        state = self.controller.pipeline_component_restarted(self.xpresso_run_name,
                                                             self.name)
        if state is not None:
            # self.state = state
            self.controller.update_pipeline_run_status(self.xpresso_run_name, "RUNNING")

        else:
            print(
                "This component already completed execution. Some other component needs to restart. Terminating",
                flush=True)
            self.terminate_without_exit()

    def completed(self):
        print(f"Parent component completed {self.name}", flush=True)
        self.controller.pipeline_component_completed(self.xpresso_run_name, self.name)
        self.is_thread_needed = False

    def report_pipeline_status(self, status):
        print("Parent component reporting status", flush=True)
        self.controller.report_pipeline_status(self.xpresso_run_name, self.name, status)

    def check_run_status(self):
        while self.is_thread_needed:
            self.get_run_status()
            time.sleep(5)
        print(f"Stopping check run status thread for {self.name}")

    def get_run_status(self):
        self.run_status = self.controller.get_pipeline_run_status(self.xpresso_run_name)
        print(
            "Run ID: {}, Time:{}, Component: {}, Status: {}".format(
                str(self.xpresso_run_name),
                datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                self.name,
                self.run_status))
        print("RUN STATUS: {}".format(self.run_status), flush=True)
        if self.run_status == "IDLE":
            pass
        elif self.run_status == "RUNNING":
            pass
        elif self.run_status == "TERMINATED":
            print("Terminating component", flush=True)
            self.terminate()
        elif self.run_status == "PAUSED":
            print("Pausing component", flush=True)
            self.pause()
        elif self.run_status == "RESTARTED":
            print("Restarting component", flush=True)
            self.restart()
        else:
            print("No action required...continuing", flush=True)


class AbstractSparkComponent(AbstractPipelineComponent):
    def __init__(self, xpresso_run_name, is_thread_needed=True):
        super().__init__(xpresso_run_name, is_thread_needed=is_thread_needed)


class AbstractSparkPipelineEstimator(AbstractSparkComponent):

    def __init__(self, xpresso_run_name, is_thread_needed=True):
        super().__init__(xpresso_run_name, is_thread_needed=True)


class AbstractSparkPipelineTransformer(AbstractSparkComponent):
    def __init__(self, xpresso_run_name, is_thread_needed=True):
        super().__init__(xpresso_run_name, is_thread_needed=True)


class XprPipeline(Pipeline, AbstractSparkComponent):

    def __init__(self, sys_args):
        parser = argparse.ArgumentParser()
        parser.add_argument('--xpresso_run_name', type=str, default='xpresso_run_name',
                            help='xpresso_run_name help')
        args, unknown = parser.parse_known_args(sys_args)
        self.name = args.xpresso_run_name
        self.xpresso_run_name = args.xpresso_run_name
        self.spark_session = SparkSession \
            .builder \
            .appName(self.xpresso_run_name) \
            .getOrCreate()
        Pipeline.__init__(self)
        AbstractSparkComponent.__init__(self, self.xpresso_run_name, is_thread_needed=False)

    def _fit(self, dataset):
        stages = self.getStages()
        for stage in stages:
            xpresso_run_name = stage.xpresso_run_name
            if not (isinstance(stage, Estimator) or isinstance(stage, Transformer)):
                raise TypeError(
                    "Cannot recognize a pipeline stage of type %s." % type(stage))

        indexOfLastEstimator = -1
        for i, stage in enumerate(stages):
            if isinstance(stage, Estimator):
                indexOfLastEstimator = i
        transformers = []
        p_start_time = time.time()
        print(f'indexOfLastEstimator={indexOfLastEstimator}')
        for i, stage in enumerate(stages):
            start_time = time.time()
            print(f'Now running stage={i} and name={stage.name} with xpresso_run_name={self.xpresso_run_name}', flush=True)
            if i <= indexOfLastEstimator:
                if isinstance(stage, Transformer):
                    transformers.append(stage)
                    dataset = stage.transform(dataset)
                else:  # must be an Estimator
                    model = stage.fit(dataset)
                    transformers.append(model)
                    if i < indexOfLastEstimator:
                        dataset = model.transform(dataset)
            else:
                transformers.append(stage)

            self.state = dataset
            stage.state = dataset
            status = {'status' : f'Component completed {stage.name}', 'metrics' : {}}
            stage.report_pipeline_status(status)
        self.completed()

        print(f'Done now returning PipelineModel', flush=True)
        return PipelineModel(transformers)